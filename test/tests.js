// Explaining some confusion about scopes and javascript
// I am doubting any of this is surprising to you.

// All in all, I think what we are seeing is just some confusing code that
// I should go back and clean up to make less confusing

'use strict';

// Scopes work just how you would think in other languages, with the
// exception of `this`, but I am sure you are aware of that
describe('Function Scopes', function(){
  var cool = 200;
  it('BEFORE: cool will still equal 200', function(){
    expect(cool).toEqual(200);
    expect(cool).not.toEqual(1);
  });
  function bob(cool){
    it('BEFORE - inside function: cool will equal what we pass in', function(){
      expect(cool).toEqual(1);
      expect(cool).not.toEqual(200);
    });
    /**
     * I would never expect to use or see this behaviour outside of angular's
     * inject method from their mock suite. Because angular has
     * injection "magic", it is adding this variable to the tests scope,
     * making it available to test and use it like you would in angular
     */
    cool = cool;
    it('AFTER - inside function: cool will equal what we pass in', function(){
      expect(cool).toEqual(1);
      expect(cool).not.toEqual(200);
    });
  }
  it('After: cool will still equal 200', function(){
    expect(cool).toEqual(200);
    expect(cool).not.toEqual(1);
  });
  // Call function under test
  bob(1);
});
describe('Function Scopes Part Two', function(){
  // synchronous test
  it("Should act normally", function(){
    var _cool = 200;
    expect(_cool).toEqual(200);
    expect(_cool).not.toEqual(1);
    function bob(cool){
      expect(_cool).toEqual(200);
      expect(_cool).not.toEqual(1);
      _cool = cool;
      expect(_cool).toEqual(1);
      expect(_cool).not.toEqual(200);
    }
    expect(_cool).toEqual(200);
    expect(_cool).not.toEqual(1);
    // Call function under test
    bob(1);
    expect(_cool).toEqual(1);
    expect(_cool).not.toEqual(200);
  });
});

describe('testService', function() {
  var _testService;
  //load the module.
  module('app');
  //inject your service for testing.
  inject(function(testService) {
    _testService = testService;
    expect(_testService.users[0].name).toEqual("bob");
  });
  expect(_testService).toEqual(undefined);
  describe('testService two', function() {
    var testService;
    expect(testService).toEqual(undefined);
    beforeEach(function (){
      //load the module.
      module('app');
      inject(function(testService) {
        // In reality, we don't even need to assign `testService` to anything, angularjs
        // injection handles this
      });
    });
    expect(testService).toEqual(undefined);
    it('Testing Service', inject(function(testService) {
      expect(testService.users[0].name).toEqual("bob");
    }));
    expect(testService).toEqual(undefined);
  });
});

describe('Other', function(){
  var _cool;
  beforeEach(function (){
    _cool = 200;
  });
  it("continued", function(){
    _cool = _cool + 100;
    expect(_cool).toEqual(300);
  });
  it("continued", function(){
    var a = 10;
    var b = function(a){
      a = a + 100;
      return a;
    };
    b(200);
    expect(a).toEqual(10);
    expect(b(200)).toEqual(300);
    //Nor is a global
    expect(window.a).toEqual(undefined);
  });
});