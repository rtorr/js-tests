
var app = angular.module('app',[]);

app.controller('testController', function(testService){
  $scope.user = testService.users[0].name;
});

app.service('testService', function(){
 this.users = [
   {
     id: 1,
     name: "bob"
   }
 ];
});
