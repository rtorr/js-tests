# Just some Javascript tests

Please read the notes in `test/tests.js`.

### Install dependencies

    npm install

### Run Tests

    npm test

You can also set up webstorm-7+ or intellij-13+ to run the karma tests.